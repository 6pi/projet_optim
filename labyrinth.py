from visit import *
import csv
import sys

class Labyrinth2D:
	"""
	Class to storea 2D Labyrinth by specifying a truth value for each possible wall

	Attributes:
	  n and m are the labyrinth dimension
	  verticalWalls: n-1 x m array
		verticalWalls[i][j] is True iff there is a wall between (i, j) and (i+1, j)
	  horizontalWalls: n x m-1 array
		horizontalWalls[i][j] is True iff there is a wall between (i, j) and (i, j+1)
	"""

	def __init__(self, n, m, type):
		"""Create a new labyrinth with all walls present"""

		self.n = n
		self.m = m
		if (type == "base"):
			self.verticalWalls = [ [True] * self.m for i in range(self.n-1) ]
			self.horizontalWalls = [ [True] * (self.m-1) for i in range(self.n) ]
			print(self.verticalWalls)
			print(self.horizontalWalls)
		elif (type == "hunt"):
			self.hunt_and_kill()
		elif (type == "backtrack"):
			self.backtrack_gen()
		else:
			print("Type not available.")

	#________________ DIVISION ____________________

	def orientation(self, width, height):
		if (width > height):
			hor = True
		elif (width < height):
			hor = False
		else:
			if (random.randint(0, 1) == 0):
				hor = True
			else:
				hor = False
		return hor

	def recursive_division(self):
		self.verticalWalls = [ [False] * self.m for i in range(self.n-1) ]
		self.horizontalWalls = [ [False] * (self.m-1) for i in range(self.n) ]
		self.division(0, 0, self.n, self.m)
		return

	def division(self, x, y, width, height):
		if (height <= 2 or width <= 2):
			return
		hor = self.orientation(width, height);
		if (hor == False):
			self.horizontal_division(x, y, width, height)
		else:
			self.vertical_division(x, y, width, height)

	def vertical_division(self, x, y, width, height):
		print("vertical")
		wall = random.randint(0, width - 2)
		passage = random.randint(0, height - 1)

		for i in range(0, height):
			if (i != passage):
				self.verticalWalls[x + wall][i] = True;

		self.printCompact()
		nx = x + wall
		right_width = width - (nx - x)
		left_width = width - right_width
		self.division(x, y, left_width, height)
		self.division(nx, y, right_width, height)
		return

	def horizontal_division(self, x, y, width, height):
		print("horizontal")
		wall = random.randint(0, height - 2)
		passage = random.randint(0, width - 1)

		for i in range(0, width):
			if (i != passage):
				self.horizontalWalls[i][y + wall] = True;
		self.printCompact()
		ny = y + wall
		bottom_height = height - (ny - y)
		top_height = height - bottom_height
		self.division(x, y, width, top_height)
		self.division(x, ny, width, bottom_height)
		return

	# ________________ END DIVISION _______________

	def backtrack_gen_func(self, pos):
		"""
			backtracking function
			- set the cell in the visited array as True
			- search for neighbors that are unvisited
			- break the wall between the neighbor and the current cell
			- call the backtracking function at the neighbor position
		"""
		self.visited[pos[1]][pos[0]] = True
		neighbors = self.hunt_neighbors(pos, False)
		random.shuffle(neighbors)
		print(pos)
		for i in self.visited:
			print(i)

		for i in neighbors:
			if (self.visited[i[1]][i[0]] == False):
				self.break_wall(pos, i)
				self.backtrack_gen_func(i)


	def backtrack_gen(self):
		"""
			Main backtrack function
			- set all the wall to True,
			- create a new array of dimension width height all set to False
			- set up randomly the begining position
			- call the backtracking function
		"""

		self.verticalWalls = [ [True] * self.m for i in range(self.n-1) ]
		self.horizontalWalls = [ [True] * (self.m-1) for i in range(self.n) ]
		y = random.randint(0, self.m - 1)
		x = random.randint(0, self.n - 1)
		self.visited = [[False for i in range(0, self.n)] for j in range(0, self.m)]
		self.backtrack_gen_func((x, y))
		self.printCompact()



	# ________________ HUNT _______________________

	def break_wall(self, pos1, pos2):
		"""set the wall between pos1 and pos2 to False"""
		x1 = pos1[0]
		y1 = pos1[1]
		x2 = pos2[0]
		y2 = pos2[1]
		if (x1 > x2):
			self.verticalWalls[x2][y1] = False
		elif (x2 > x1):
			self.verticalWalls[x1][y1] = False
		elif (y1 > y2):
			self.horizontalWalls[x1][y2] = False
		else:
			self.horizontalWalls[x1][y1] = False
		return

	def hunt_neighbors(self, pos, bool):
		"""return the neighbors equal to bool of pos"""
		x, y = pos
		result = []
		if (x > 0):
			if (self.visited[y][x - 1] == bool):
				result.append((x - 1, y))
		if (y > 0):
			if (self.visited[y - 1][x] == bool):
				result.append((x, y - 1))
		if (x < self.n - 1):
			if (self.visited[y][x + 1] == bool):
				result.append((x + 1, y))
		if (y < self.m - 1):
			if (self.visited[y + 1][x] == bool):
				result.append((x, y + 1))
		return result

	def walk(self, x, y):
		"""randomly choose a neighbors of (x, y)"""
		neighbors = self.hunt_neighbors((x, y), False)
		if (neighbors == []):
			return (-1, -1)
		else:
			return (neighbors[random.randint(0, len(neighbors) - 1)])


	def hunt(self):
		""" parse the maze too find a unvisited pos next too a visited one"""
		y = 0
		while (y < self.m):
			x = 0
			while (x < self.n):
				if (self.visited[y][x] == False):
					neighbors = self.hunt_neighbors((x, y), True)
					if (neighbors != []):
						return (neighbors[random.randint(0, len(neighbors) - 1)])
				x += 1
			y += 1
		return (-1, -1)


	def hunt_and_kill(self):
		"""Main function, """
		self.verticalWalls = [ [True] * self.m for i in range(self.n-1) ]
		self.horizontalWalls = [ [True] * (self.m-1) for i in range(self.n) ]
		y = random.randint(0, self.m - 1)
		x = random.randint(0, self.n - 1)
		self.visited = [[False for i in range(0, self.n)] for j in range(0, self.m)]
		self.visited[y][x] = True
		stop = False
		while (not stop):
			nx, ny = self.walk(x, y)
			while (nx != -1):
				self.visited[ny][nx] = True
				self.break_wall((x, y), (nx, ny))
				x , y = nx, ny
				nx, ny = self.walk(x, y)
			nx, ny = self.hunt()
			if (nx == -1):
				break
			self.visited[ny][nx] = True
			self.break_wall((x, y), (nx, ny))
			x, y = nx, ny
		return
	# ________________ END HUNT ___________________

	# ________________ SOLVE ______________________

	def backtrack(self, pos, dist):
		"""
			Solve backtrack function
			- Check if the target is reach, change the shortest distance found if needed
			- Check if the actual distance is under the shortest distance found
				- if yes : stop the backtrack at this point
			- set the actual cell as visited
			- Get its neighbors
			- call backtrack for each or its neighbors unvisited
		"""
		x, y = pos

		if (pos == self.target):
			if (self.shortest == -1 or self.shortest > dist):
				self.shortest = dist
				self.visited[x][y] = False
				return

		if (self.shortest != -1 and self.shortest < dist):
			self.visited[x][y] = False
			return

		self.visited[x][y] = True

		neighbors = self.neighbors((x, y))

		for i, j in neighbors:
			if (self.visited[i][j] == False):
				self.backtrack((i, j), dist + 1)

		self.visited[x][y] = False

	def solve(self):
		"""
			Main solve funtion
			set the target and create a array of dimension width height with all cells set to False
			call backtrack function
		"""
		self.target = (self.n - 1, self.m - 1)
		self.visited = [[False] * self.m for i in range(self.n)]
		self.shortest = -1
		self.backtrack((0, 0), 1)
		print(self.shortest)

	# ________________ END SOLVE __________________

	def saveCSV(self, file):
		"""Save the labyrinth to file in CSV format"""
		with open(file, 'w') as File:
			writer = csv.writer(File, delimiter=' ')
			writer.writerow((self.n, self.m))
			for i in range(self.n - 1):
				for j in range(self.m):
					if self.verticalWalls[i][j]: writer.writerow((i, j, 'V'))
			for i in range(self.n):
				for j in range(self.m-1):
					if self.horizontalWalls[i][j]: writer.writerow((i, j, 'H'))

	def neighbors(self, pos):
		"""Return the list of neighboring cells of position pos"""

		i, j = pos
		result = []
		if i > 0:
			if not self.verticalWalls[i-1][j]: result.append((i-1, j))
		if j > 0:
			if not self.horizontalWalls[i][j-1]: result.append((i, j-1))
		if i < self.n-1:
			if not self.verticalWalls[i][j]: result.append((i+1, j))
		if j < self.m-1:
			if not self.horizontalWalls[i][j]: result.append((i, j+1))
		assert result, f"Empty neighbor list for position {self.currentPosition}"
		return result


	def printCompact(self):
		"""Print the labyrinth as text, compact form"""

		table = { (True, True, True, True) : '┼',
				  (True, True, True, False) : '┴',
				  (True, True, False, True) : '┤',
				  (True, True, False, False) : '┘',
				  (True, False, True, True) : '┬',
				  (True, False, True, False) : '─',
				  (True, False, False, True) : '┐',
				  (True, False, False, False) : '╴',
				  (False, True, True, True) : '├',
				  (False, True, True, False) : '└',
				  (False, True, False, True) : '│',
				  (False, True, False, False) : '╵',
				  (False, False, True, True) : '┌',
				  (False, False, True, False) : '╶',
				  (False, False, False, True) : '╷',
				  (False, False, False, False) : ' '
				  }
		# Specific logic to include walls around the labyrinth
		def isVertWall(i, j):
			if i >= 0 and j >= 0 and i < self.n - 1 and j < self.m:
				return self.verticalWalls[i][j]
			return (i == -1 or i == self.n-1) and (j>= 0 and j < self.m)
		def isHorizWall(i, j):
			if i >= 0 and j >= 0 and i < self.n  and j < self.m - 1:
				return self.horizontalWalls[i][j]
			return (j == -1 or j == self.m-1) and (i>= 0 and i < self.n)

		def getWalls(i, j):
			return (isHorizWall(i-1, j-1),
					isVertWall(i-1, j-1),
					isHorizWall(i, j-1),
					isVertWall(i-1, j))

		for j in range(self.m + 1):
			print("".join(table[getWalls(i, j)] for i in range(self.n + 1)))

	def printWide(self):
		"""Print the labyrinth as text, wider form"""

		wallChar = '█'
		noWallChar = ' '
		cellChar = ' '
		def printLine(j):
			print(cellChar.join(wallChar if i == 0 or i == self.n or self.verticalWalls[i-1][j] else noWallChar
								for i in range(self.n+1)))

		print(wallChar * (2*self.n + 1))
		for j in range(self.m-1):
			printLine(j)
			print(wallChar + wallChar.join(wallChar if self.horizontalWalls[i][j] else noWallChar
										   for i in range(self.n)) + wallChar)
		printLine(self.m - 1)
		print(wallChar * (2*self.n + 1))


class Labyrinth2DFromFile(Labyrinth2D):
	"""Labyrinth class created by reading from a file in CSV format"""

	def __init__(self, file):
		with open(file, 'rt') as csvfile:
			reader = csv.reader(csvfile, delimiter = ' ')
			try:
				rowSize = next(reader)
				line = 1
				if len(rowSize) != 2:
					raise ValueError(f'First Line should have length 2, not {len(rowSize)}')
				self.n = int(rowSize[0])
				self.m = int(rowSize[1])
				self.verticalWalls = [ [False] * self.m for i in range(self.n-1) ]
				  # verticalWalls[i][j] is True iff there is a wall between (i, j) and (i+1, j)
				self.horizontalWalls = [ [False] * (self.m-1) for i in range(self.n) ]
				  # horizontalWalls[i][j] is True iff there is a wall between (i, j) and (i, j+1)
				for row in reader:
					line += 1
					if len(row) != 3:
						raise ValueError(f'Lines after the first should have length 3, not {len(rowSize)}')
					i = int(row[0])
					j = int(row[1])
					t = row[2]
					if t == 'V':
						if i >= 0 and i < self.n-1 and j >= 0 and j < self.m:
							self.verticalWalls[i][j] = True
						else:
							raise ValueError(f'Position {i}, {j} is not valid for a vertical wall')
					elif t == 'H':
						if i >= 0 and i < self.n and j >= 0 and j < self.m-1:
							self.horizontalWalls[i][j] = True
						else:
							raise ValueError(f'Position {i}, {j} is not valid for a horizontal wall')
					else:
						raise ValueError(f'Type {t} is not valid, only V and H allowed')
			except ValueError as e:
				raise ValueError(f"Line {line}, error: {e}")

def man_visit():
	print("If you awant to visit a maze:")
	print("python3 labyrinth.py visit {maze file}\n")

def man_solve():
	print("If you want to solve a maze:")
	print("python3 labyrinth.py solve {maze file}\n")

def man_generate():
	print("If you want to generate a maze:")
	print("python3 labyrinth.py generate {width} {height} {algorithm} {outpout maze file}\n")

def man_all():
	man_visit()
	man_solve()
	man_generate()

if (len(sys.argv) < 3):
	print("Not enough arguments. Execute program like this :\n")
	man_all()
elif (sys.argv[1] == "solve"):
	if (len(sys.argv) != 3):
		man_solve()
	else:
		lab = Labyrinth2DFromFile(sys.argv[2])
		lab.solve()
elif (sys.argv[1] == "visit"):
	if (len(sys.argv) != 3):
		man_visit()
	else:
		lab = Labyrinth2DFromFile(sys.argv[2])
		lab.printCompact()
		vis = Visit(lab)
elif (sys.argv[1] == "generate"):
	if (len(sys.argv) == 6 and sys.argv[2].isnumeric() and sys.argv[3].isnumeric() and (sys.argv[4] == "backtrack" or sys.argv[4] == "hunt")):
		lab = Labyrinth2D(int(sys.argv[2]), int(sys.argv[3]), sys.argv[4])
		lab.printWide()
		lab.printCompact()
		lab.saveCSV(sys.argv[5])
else:
	man_all()
